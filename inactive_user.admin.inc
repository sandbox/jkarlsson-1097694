<?php

/**
 * Custom settings page: menu callback
 *
 * (we're using a custom callback to enable a nicer menu title,
 * without underscore)
 */
function inactive_user_custom_settings() {
  $period = array(0 => 'disabled') + drupal_map_assoc(array(604800, 1209600, 1814400, 2419200, 2592000, 7776000, 15552000, 23328000, 31536000, 47088000, 63072000), '_format_interval');
  $warn_period = array(0 => 'disabled') + drupal_map_assoc(array(86400, 172800, 259200, 604800, 1209600, 1814400, 2592000), '_format_interval');
  $mail_variables = ' %username, %useremail, %lastaccess, %period, %sitename, %siteurl';

  //protected roles
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  $form['inactive_user_protected_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Protected roles'),
    '#default_value' => variable_get('inactive_user_protected_roles', array()),
    '#description' => t('Check the roles will be unaffected by this module.'),
    '#options' => $roles,
  );
  // set administrator e-mail
  $form['inactive_user_admin_email_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Administrator e-mail'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['inactive_user_admin_email_fieldset']['inactive_user_admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail addresses'),
    '#default_value' => _inactive_user_admin_mail(),
    '#description' => t('Supply a comma-separated list of e-mail addresses that will receive administrator alerts. Spaces between addresses are allowed.'),
    '#maxlength' => 256,
    '#required' => TRUE,
  );

  // inactive user notification
  $form['inactive_user_notification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inactive user notification'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['inactive_user_notification']['inactive_user_notify_admin'] = array(
    '#type' => 'select',
    '#title' => t('Notify administrator when a user hasn\'t logged in for more than'),
    '#default_value' => variable_get('inactive_user_notify_admin', 0),
    '#options' => $period,
    '#description' => t('Generate an email to notify the site administrator that a user account hasn\'t been used for longer than the specified amount of time.  Requires crontab.'),
  );
  $form['inactive_user_notification']['inactive_user_notify'] = array(
    '#type' => 'select',
    '#title' => t('Notify users when they haven\'t logged in for more than'),
    '#default_value' => variable_get('inactive_user_notify', 0),
    '#options' => $period,
    '#description' => t('Generate an email to notify users when they haven\'t used their account for longer than the specified amount of time.  Requires crontab.'),
  );
  $form['inactive_user_notification']['inactive_user_notify_include_blocked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify blocked users'),
    '#default_value' => variable_get('inactive_user_notify_include_blocked', 1),
    '#description' => t('Also send email to blocked users.'),
  );
  $form['inactive_user_notification']['inactive_user_notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user notification e-mail'),
    '#default_value' => variable_get('inactive_user_notify_text', _inactive_user_mail_text('notify_text')),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user.') .' '. t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );

  // automatically block inactive users
  $form['block_inactive_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatically block inactive users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['block_inactive_user']['inactive_user_auto_block'] = array(
    '#type' => 'select',
    '#title' => t('Block users that haven\'t logged in for more than'),
    '#default_value' => variable_get('inactive_user_auto_block', 0),
    '#options' => $period,
    '#description' => t('Automatically block user accounts that haven\'t been used in the specified amount of time.  Requires crontab.'),
  );
  $form['block_inactive_user']['inactive_user_notify_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user'),
    '#default_value' => variable_get('inactive_user_notify_block', 0),
    '#description' => t('Generate an email to notify a user that his/her account has been automatically blocked.'),
  );
  $form['block_inactive_user']['inactive_user_block_notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of blocked user acount e-mail'),
    '#default_value' => variable_get('inactive_user_block_notify_text', _inactive_user_mail_text('block_notify_text')),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account has been blocked.') .' '. t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['block_inactive_user']['inactive_user_notify_block_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify administrator'),
    '#default_value' => variable_get('inactive_user_notify_block_admin', 0),
    '#description' => t('Generate an email to notify the site administrator when a user is automatically blocked.'),
  );
  $form['block_inactive_user']['inactive_user_auto_block_warn'] = array(
    '#type' => 'select',
    '#title' => t('Warn users before they are blocked'),
    '#default_value' => variable_get('inactive_user_auto_block_warn', 0),
    '#options' => $warn_period,
    '#description' => t('Generate an email to notify a user that his/her account is about to be blocked.'),
  );
  $form['block_inactive_user']['inactive_user_block_warn_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user warning e-mail'),
    '#default_value' => variable_get('inactive_user_block_warn_text', _inactive_user_mail_text('block_warn_text')),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account is about to be blocked.') .' '. t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );

  // automatically delete inactive users
  $form['delete_inactive_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatically delete inactive users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['delete_inactive_user']['inactive_user_auto_delete'] = array(
    '#type' => 'select',
    '#title' => t('Delete users that haven\'t logged in for more than'),
    '#default_value' => variable_get('inactive_user_auto_delete', 0),
    '#options' => $period,
    '#description' => t('Automatically delete user accounts that haven\'t been used in the specified amount of time.  Warning, user accounts are permanently deleted, with no ability to undo the action!  Requires crontab.'),
  );
  $form['delete_inactive_user']['inactive_user_preserve_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve users that own site content'),
    '#default_value' => variable_get('inactive_user_preserve_content', 1),
    '#description' => t('Select this option to never delete users that own site content.  If you delete a user that owns content on the site, such as a user that created a node or left a comment, the content will no longer be available via the normal Drupal user interface.  That is, if a user creates a node or leaves a comment, then the user is deleted, the node and/or comment will no longer be accesible even though it will still be in the database.'),
  );
  $form['delete_inactive_user']['inactive_user_notify_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user'),
    '#default_value' => variable_get('inactive_user_notify_delete', 0),
    '#description' => t('Generate an email to notify a user that his/her account has been automatically deleted.'),
  );
  $form['delete_inactive_user']['inactive_user_notify_delete_include_blocked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify blocked users'),
    '#default_value' => variable_get('inactive_user_notify_delete_include_blocked', 1),
    '#description' => t('If unchecked blocked users will also receive notifications.'),
  );
  $form['delete_inactive_user']['inactive_user_delete_notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of deleted user account e-mail'),
    '#default_value' => variable_get('inactive_user_delete_notify_text', _inactive_user_mail_text('delete_notify_text')),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account has been deleted.') .' '. t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['delete_inactive_user']['inactive_user_notify_delete_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify administrator'),
    '#default_value' => variable_get('inactive_user_notify_delete_admin', 0),
    '#description' => t('Generate an email to notify the site administrator when a user is automatically deleted.'),
  );
  $form['delete_inactive_user']['inactive_user_auto_delete_warn'] = array(
    '#type' => 'select',
    '#title' => t('Warn users before they are deleted'),
    '#default_value' => variable_get('inactive_user_auto_delete_warn', 0),
    '#options' => $warn_period,
    '#description' => t('Generate an email to notify a user that his/her account is about to be deleted.')
  );
  $form['delete_inactive_user']['inactive_user_auto_delete_warn_include_blocked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Warn blocked users'),
    '#default_value' => variable_get('inactive_user_auto_delete_warn_include_blocked', 1),
    '#description' => t('If unchecked blocked users will not be warned.'),
  );
  $form['delete_inactive_user']['inactive_user_delete_warn_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user warning e-mail'),
    '#default_value' => variable_get('inactive_user_delete_warn_text', _inactive_user_mail_text('delete_warn_text')),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account is about to be deleted.') .' '. t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

function inactive_user_custom_settings_validate($form, &$form_state) {
  $mails = explode(',', $edit['inactive_user_admin_email']);
  foreach ($mails as $mail) {
    if ($mail && !valid_email_address(trim($mail))) {
      $invalid[] = $mail;
      $count++;
    }
  }
  if ($count ==  1) {
    form_set_error('inactive_user_admin_email', t('%mail is not a valid e-mail address', array('%mail' => $invalid[0])));
  }
  elseif ($count > 1) {
    form_set_error('inactive_user_admin_email', t('The following e-mail addresses are invalid: %mail', array('%mail' => implode(', ', $invalid))));
  }
}

?>